package com.example.demo;

import net.minidev.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;
import java.util.stream.Stream;


public class Activities {
    String message;
    JSONObject json;


    public Activities() {
        message = "";
        json = new JSONObject();
    }

    public void getData() {
        String fileName = "D:\\DS\\assignment2\\activity.txt";
        RabbitConfig config = new RabbitConfig();
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

            stream.forEach(s->{
                String split[] = s.split("\t\t");
                Random rn = new Random();
                int answer = rn.nextInt(10) + 1;
                json.put("patient_id", answer);
                json.put("activity", split[2]);
                json.put("start", split[0]);
                json.put("end", split[1]);

                message = json.toString();
                try {
                    config.connection(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.out.println(message);
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

