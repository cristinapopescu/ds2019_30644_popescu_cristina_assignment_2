package com.example.springdemo.services;

import com.example.springdemo.entities.Activity;
import com.example.springdemo.repositories.ActivityRepository;
import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Component
public class RabbitConfig implements MessageListener {
    private final static String QUEUE_NAME = "carlas";

    @Autowired
    private final ActivityRepository activityRepository;
    WebSocketConfig wsc = new WebSocketConfig();

    public RabbitConfig(ActivityRepository activityRepository){
        wsc.Connect("ws://localhost:3030/");
        this.activityRepository = activityRepository;
    }

//    public void connect() {
//        ConnectionFactory factory = new ConnectionFactory();
//        factory.setHost("localhost");
//        Connection connection = null;
//        try {
//            connection = factory.newConnection();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (TimeoutException e) {
//            e.printStackTrace();
//        }
//        try {
//            Channel channel = connection.createChannel();
//            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
//            System.out.println("[*] Waiting for messages. To exit press CTRL+C");
//
//            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
//                String message = new String(delivery.getBody(), "UTF-8");
//                Gson gson = new Gson();
//                Activity activity = gson.fromJson(message, Activity.class);
//                insert(activity);
//                Date start = parseDate(activity.getStart());
//                Date end = parseDate(activity.getEnd());
//                long diffInMillis = Math.abs(end.getTime() - start.getTime());
//                long diff = TimeUnit.HOURS.convert(diffInMillis, TimeUnit.MILLISECONDS);
//                if(activity.getActivity().equals("Sleeping") && diff > TimeUnit.HOURS.toMillis(12)){
//                    System.out.println("dorme mult");
//                }
//                if(activity.getActivity().equals("Leaving") && diff > TimeUnit.HOURS.toMillis(12)){
//                    System.out.println("iese mult");
//                }
//                if(activity.getActivity().equals("Toileting") && diff > TimeUnit.HOURS.toMillis(1)){
//                    System.out.println("spala mult");
//                }
//                System.out.println("[x] Received" + message + "'");
//            };
//            channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> {});
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    public Date parseDate(String date){
        SimpleDateFormat formatter6 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date1= null;
        try {
            date1 = formatter6.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date1;
    }

    public Integer insert(Activity activity) {
        return activityRepository
                .save(activity)
                .getId();
    }

    @Override
    @RabbitListener(queues = "carlas")
    public void onMessage(Message message) {
        Gson gson = new Gson();
        String json = new String(message.getBody());
        Activity activity = gson.fromJson(json, Activity.class);
        insert(activity);
        Date start = parseDate(activity.getStart());
        Date end = parseDate(activity.getEnd());
        long diffInMillis = Math.abs(end.getTime() - start.getTime());
        long diff = TimeUnit.HOURS.convert(diffInMillis, TimeUnit.MILLISECONDS);
        if(activity.getActivity().equals("Sleeping") && diff > 12){
            System.out.println("dorme mult");
            try {
                wsc.SendMessage(activity.getPatient_id() + " sleeping");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(activity.getActivity().equals("Leaving") && diff > 12){
            System.out.println("iese mult");
            try {
                wsc.SendMessage(activity.getPatient_id() + " leaving");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(activity.getActivity().equals("Toileting") && diff > 1){
            System.out.println("spala mult");
            try {
                wsc.SendMessage(activity.getPatient_id() + " toileting");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("[x] Received" + json + "'");
    }
}
